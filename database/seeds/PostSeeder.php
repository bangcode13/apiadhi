<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::create([
        		'title'=>'lorem 1',
        		'content'=>'lorem ipsum dolor sit amet',
        		'category'=>'progress',
        		'image'=>'hohoho',
        		'user_id'=>1
        	]);

        Post::create([
        		'title'=>'lorem 2',
        		'content'=>'lorem ipsum dolor sit amet',
        		'category'=>'sop',
        		'image'=>'hohoho',
        		'user_id'=>2
        	]);
    }
}
